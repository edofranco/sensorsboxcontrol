## SONARS BOX CONTROL ##

Sirve para controlar un pequeño videojuego con 4 sensores de proximidad. 
Cada uno representa una dirección. 
By: @edofranco_

***********************************

Primero debes cargar el codigo de Arduino en tu placa Arduino UNO. 
Luego los sensores se deben cargar en los pines que dice el programa. 

Cada sensor tiene 4 pines, 2 son de data, 2 son de power. 

Los 2 de power son comunes para los sensores, así que puedes usar un protoboard para unir todos los grnd y todos los 5v con los pines respectivos del arduino. 

Con respecto a los pines de cada uno, en el codigo dice dónde va el echo y donde va el trigger, es personalizable. 

Una vez que el todo esté conectado, comienza la fiesta, se debe: 

1) Descargar VJoy que permite las utilerias de:
- VJoy Monitor (para ver los botones activos)
- VJoy Conf (para crear un nuevo mando virtual)

2) Descargar vJoySerialFeeder que es quien permite convertir la salida COM del Arduino a "botones virtuales"
NOTA: La salida COM del Arduino está programada para ser 1023 cuando al distancia sea menor de 10 para cada sensor, eso quiere decir que cada boton del vJoySerialFeeder debe ser configurado en setup con un solo treshold de 1023 para que lo active. 

*** 
En este punto abre el VJoy Monitor y verás como el Arduino activa los botones que se configuraron. 
Solo falta: 

3) Descargar UCR que permite convertir los botones virtuales a acciones de teclado, es un gran invendo de HotKeys y nos ahorra mucho trabajo, solo tenemos que configurar el perfil de ButtonToButton por cada uno de los sensores y decir que "boton1" (virtual) es equivalente a su tecla del teclado (valga la redundancia). 

**********************************
Para que todo sea más fácil de retomar, he puesto los programas en: 
https://www.dropbox.com/sh/26oilo79xz4z645/AACm0oXcLT5BEWU9JFHbYntYa?dl=0

Hay una buena documentación en: https://github.com/Cleric-K/vJoySerialFeeder al cual le di un Fork en 
https://github.com/edofranco/vJoySerialFeeder
No soy creador de esas librerias y en cada código pueden ver los créditos respectivos. 

Si estás viendo esto, lo siento si no explico mejor, estamos viendo Sense8 y se va la luz, tengo hambre y vamos por pizza. 

Maracaibo, 10 de Junio, 2018. 