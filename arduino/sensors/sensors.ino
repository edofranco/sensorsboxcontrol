#include <NewPing.h>
#include "ibus.h";
/*
CODE HERE
Array of HC-SR04 ultrasonic sensors

cambio al 1.5 seg
*/

#define SONAR_NUM 4 // Number of sensors.
#define MAX_DISTANCE 130// Maximum distance (in cm) to ping.
#define PING_INTERVAL 35 // Milliseconds between sensor pings (29ms is about the min to avoid cross-sensor echo).
#define NUM_CHANNELS 4

unsigned long pingTimer[SONAR_NUM]; // Holds the times when the next ping should happen for each sensor.
unsigned int cm[SONAR_NUM];         // Where the ping distances are stored.
unsigned int last[SONAR_NUM] = {0,0,0,0};         // Where the last ping distances are stored.
unsigned char* buttons[]={"LEFT", "UP", "DOWN","RIGHT"};
char* button;

uint8_t currentSensor = 0;          // Keeps track of which sensor is active.

IBus ibus(NUM_CHANNELS);


NewPing sonar[SONAR_NUM] = {     // Sensor object array.
    NewPing(3, 2, MAX_DISTANCE),  // IZQUIERDA
    NewPing(7, 6, MAX_DISTANCE),  // ARRIBA
    NewPing(9, 8, MAX_DISTANCE),  // ABAJO
    NewPing(13, 12, MAX_DISTANCE) // DERECHA
};



  
void setup() {
  Serial.begin(9600);
  pingTimer[0] = millis() + 75;           // First ping starts at 75ms, gives time for the Arduino to chill before starting.
  for (uint8_t i = 1; i < SONAR_NUM; i++) // Set the starting time for each sensor.
    pingTimer[i] = pingTimer[i - 1] + PING_INTERVAL;
}

void loop() {
  for (uint8_t i = 0; i < SONAR_NUM; i++) { // Loop through all the sensors.
    if (millis() >= pingTimer[i]) {         // Is it this sensor's time to ping?
      pingTimer[i] += PING_INTERVAL * SONAR_NUM;  // Set next time this sensor will be pinged.
      if (i == 0 && currentSensor == SONAR_NUM - 1) 
        oneSensorCycle(); // Sensor ping cycle complete, do something with the results.
      sonar[currentSensor].timer_stop();          // Make sure previous timer is canceled before starting a new ping (insurance).
      currentSensor = i;                          // Sensor being accessed.
      cm[currentSensor] = MAX_DISTANCE;                     // Make distance zero in case there's no ping echo for this sensor.
      sonar[currentSensor].ping_timer(echoCheck); // Do the ping (processing continues, interrupt will call echoCheck to look for echo).
    }
  }
  // Other code that *DOESN'T* analyze ping results can go here.
  ibus.begin();
  for(uint8_t i = 0; i < NUM_CHANNELS; i++)
      ibus.write(last[i]);
  ibus.end();

}

void echoCheck() { // If ping received, set the sensor distance to array.
  if (sonar[currentSensor].check_timer())
    cm[currentSensor] = sonar[currentSensor].ping_result / US_ROUNDTRIP_CM;
}

void oneSensorCycle() { // Sensor ping cycle complete, do something with the results.
  // The following code would be replaced with your code that does something with the ping results.
  for (uint8_t i = 0; i < SONAR_NUM; i++) {
   if(cm[i] != MAX_DISTANCE )
      last[i] = cm[i];

   //button = 0;
   if(last[i]<10){
    last[i] = 1023;
    //    button = buttons[i];
    //    Keyboard.press('t');
    //    delay(100);
    //    Keyboard.releaseAll();
   }

   //RESULTS
   // Serial.print(button);
   // if(i != (SONAR_NUM-1))
   //  Serial.print(","); 
   // else
   //   Serial.println("");
  
  } 

}



